package split_test

import (
	"errors"
	"reflect"
	"testing"

	"codeberg.org/gruf/go-split"
)

var splitTests = []struct {
	input  string   // input string
	reterr string   // return error from split func
	experr string   // expected error
	split  []string // expected string split
}{
	{
		// check the simplest case of a single string
		input:  `hello world`,
		reterr: ``,
		experr: ``,
		split:  []string{`hello world`},
	},
	{
		// check that a simple comma-space separated string is separated as expected
		input:  `1, 2, 3, 4, 5`,
		reterr: ``,
		experr: ``,
		split:  []string{`1`, `2`, `3`, `4`, `5`},
	},
	{
		// check that double quotes are correctly stripped
		input:  `"1","2","3","4","5"`,
		reterr: ``,
		experr: ``,
		split:  []string{`1`, `2`, `3`, `4`, `5`},
	},
	{
		// check that single quotes are correctly stripped
		input:  `'1','2','3','4','5'`,
		reterr: ``,
		experr: ``,
		split:  []string{`1`, `2`, `3`, `4`, `5`},
	},
	{
		// check that mix of single+double quotes are correctly stripped
		input:  `'1',"2",3,"4",'5',`,
		reterr: ``,
		experr: ``,
		split:  []string{`1`, `2`, `3`, `4`, `5`},
	},
	{
		// check that spaces between quoted entries is ignored
		input:  `'hello world', 'lots of spaces',    'woah'`,
		reterr: ``,
		experr: ``,
		split:  []string{`hello world`, `lots of spaces`, `woah`},
	},
	{
		// check that initial spaces (even unquoted) between entries is ignored
		input:  `hello world, lots of spaces,    woah`,
		reterr: ``,
		experr: ``,
		split:  []string{`hello world`, `lots of spaces`, `woah`},
	},
	{
		// check that unended quote returns error
		input:  `'unended quote`,
		reterr: ``,
		experr: `missing end quote`,
		split:  nil,
	},
	{
		// check that unfinished quote with multiple items returns error
		input:  `"hello world","unended quote`,
		reterr: ``,
		experr: `missing end quote`,
		split:  []string{`hello world`},
	},
	{
		input:  `something something missing beginning quote', 'hello world'`,
		reterr: ``,
		experr: ``,
		split:  []string{`something something missing beginning quote'`, `hello world`},
	},
	{
		// checked that quoted commas are not counted in splitting
		input:  `'quoted, with, commas', "quoted, with, commas"`,
		reterr: ``,
		experr: ``,
		split:  []string{`quoted, with, commas`, `quoted, with, commas`},
	},
	{
		// check that quoting with missing comma separator returns error
		input:  `"missing""separator"`,
		reterr: ``,
		experr: `missing comma separator`,
		split:  []string{`missing`},
	},
	{
		// check that escaped quotes are unescaped as expected, and that only matching quote types need escaping
		input:  `"escaped \"quoting\"",'escaped \'quoting\'',"no need to 'escape'",'no need to "escape"'`,
		reterr: ``,
		experr: ``,
		split:  []string{`escaped "quoting"`, `escaped 'quoting'`, `no need to 'escape'`, `no need to "escape"`},
	},
	{
		// check that escaped outer quotes (no matter how many) aren't treated as quoting
		input:  `\\"escaped \\\"quoting\\\"\\",\\'escaped \\\'quoting\\\'\\'`,
		reterr: ``,
		experr: ``,
		split:  []string{`\\"escaped \\\"quoting\\\"\\"`, `\\'escaped \\\'quoting\\\'\\'`},
	},
	{
		// check that escaped quotes are unescaped to the count as expected
		input:  `"escaped \\\"quoting\\\"",  'escaped \\\'quoting\\\'',  "no need to 'escape'", 'no need to "escape"'`,
		reterr: ``,
		experr: ``,
		split:  []string{`escaped \"quoting\"`, `escaped \'quoting\'`, `no need to 'escape'`, `no need to "escape"`},
	},
	{
		// check that returning an error passes through
		input:  `returns error`,
		reterr: `oh no!`,
		experr: `oh no!`,
		split:  nil,
	},
	{
		// the error during first splitting will be returned before reached missing separator
		input:  `"returns error but""also missing separator"`,
		reterr: `oh no!`,
		experr: `oh no!`,
		split:  nil,
	},
}

func TestSplitFunc(t *testing.T) {
	for _, test := range splitTests {
		var rcv []string
		t.Logf("input=\"%s\"", test.input)
		if err := split.SplitFunc(test.input, func(s string) error {
			if test.reterr != "" {
				return errors.New(test.reterr)
			}
			rcv = append(rcv, s)
			return nil
		}); (err == nil && test.experr != "") || (err != nil && test.experr != err.Error()) {
			t.Errorf("error return not as expected: result=\"%v\" expect=\"%v\"", err, test.experr)
			continue
		} else if !reflect.DeepEqual(rcv, test.split) {
			t.Errorf("split not as expected: result=\"%v\" expect=\"%v\"", rcv, test.split)
			continue
		}
	}
}
