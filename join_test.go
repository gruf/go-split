package split_test

import (
	"testing"

	"codeberg.org/gruf/go-split"
)

func TestJoinStrings(t *testing.T) {
	const expect = `hello, world, "hello, world", "\"hello world\"", "hello\nworld"`

	if out := split.JoinStrings([]string{
		"hello", "world", `hello, world`, `"hello world"`, "hello\nworld",
	}); out != expect {
		t.Errorf("joining string slice didn't return expected string:\nout    = %s\nexpect = %s", out, expect)
	}

	var empty []string

	if split.JoinStrings(empty) != "" {
		t.Error("joining nil string slice didn't return empty string")
	}
}

func TestJoinInts(t *testing.T) {
	const expect = "-4, -3, -2, -1, 0, 1, 2, 3, 4"

	if out := split.JoinInts([]int{
		-4, -3, -2, -1, 0, 1, 2, 3, 4,
	}); out != expect {
		t.Errorf("joining int slice didn't return expected string: out=%q expect=%q", out, expect)
	}

	var empty []int

	if split.JoinInts(empty) != "" {
		t.Error("joining nil int slice didn't return empty string")
	}
}

func TestJoinUints(t *testing.T) {
	const expect = "0, 1, 2, 3, 4, 5, 6, 7, 8, 9"

	if out := split.JoinUints([]uint{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
	}); out != expect {
		t.Errorf("joining uint slice didn't return expected string: out=%q expect=%q", out, expect)
	}

	var empty []uint

	if split.JoinUints(empty) != "" {
		t.Error("joining nil uint slice didn't return empty string")
	}
}

func TestJoinFloats(t *testing.T) {
	const expect = "-4.4, -3.3, -2.2, -1.1, 0, 1.1, 2.2, 3.3, 4.4"

	if out := split.JoinFloats([]float32{
		-4.4, -3.3, -2.2, -1.1, 0, 1.1, 2.2, 3.3, 4.4,
	}); out != expect {
		t.Errorf("joining float slice didn't return expected string: out=%q expect=%q", out, expect)
	}

	var empty []float32

	if split.JoinFloats(empty) != "" {
		t.Error("joining nil float slice didn't return empty string")
	}
}
